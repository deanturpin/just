# Just enough GIT to get by

```bash
git clone
git branch
git checkout -b new-branch-name # based on current branch
git pull
git push
git rebase
git merge
git pull --rebase --autostash
```
