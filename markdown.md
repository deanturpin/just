# Just enough MARKDOWN to get by
Markdown is now my go-to format. It's not as expressive as HTML but it's quick to type and very straightforward to generated programatically (you don't have to match tags).

So let's start with some structure.

## Headings
Declare a heading by prefixing up to five hashes. The hashes are equivalent to HTML `<h1>` heading tags, I like to use the single hash only once at the top.

```markdown
# h1
## h2
### h3
#### h4
##### h5
```

## Links
Next you'll want to link to another page. Let's credit [Pascal Hertleif](https://gist.github.com/killercup) for [this awesome gist](https://gist.github.com/killercup/5917178) which the CSS for this site is based on.

It takes a while to remember which comes first: the square or the round brackets. In fact, you will never remember.
```markdown
[Pascal Hertleif](https://gist.github.com/killercup)
```

### Linking to images
You link to images in the same way but prefix a pling, note you can leave the leading brackets empty.

```markdown
![](https://ping.germs.dev/hosts.svg)
```

If you want to also link to that image it starts to look a little unweildy! But basically you're adding an image to the first set of brackets.
```markdown
[![](https://ping.germs.dev/hosts.svg)](https://ping.germs.dev/hosts.svg)
```

[![](https://ping.germs.dev/hosts.svg)](https://ping.germs.dev/hosts.svg)

## Preformatted text
Often used as "don't format this text!" and comes in span or block style. Use single or triple backticks around your code and you can specify the language too.

Using:
```
```cpp
```

```cpp
int main() {
    // cya
    return 0;
}
```

Using:
```
```python
```

```python
int main() {
    // cya
    return 0;
}
```

Or a single pair of back ticks around a word like `mkdir`.

## Paragraph breaks
A blank line makes a paragraph. If you really want a new line but don't want to use bullets you can add a double space at the end of the line. But this is difficult to see/manage.

## Bullet points
A leading hyphen makes a bullet point.
```
- once
- twice
- thrice
```
- one
- two
- three

Or use a number for an enumerated list. Note you don't have to increment the numbers yourself.

```
1. once
1. twice
1. thrice
```

1. one
1. two
1. three

## Tables
```
|Beats per second | Beats per hour |
|---|---|
| 3	| 10800|
| 4	| 14400|
| 5	| 18000|
| 6	| 21600|
| 7	| 25200|
| 8	| 28800|
```

Makes a nice table like this.

|Beats per second | Beats per hour |
|---|---|
| 3	| 10800|
| 4	| 14400|
| 5	| 18000|
| 6	| 21600|
| 7	| 25200|
| 8	| 28800|


## Horizontals rule
Three hyphens make a rule, akin to `<hr>`.
```
---
```
---

<!--

## BONUS FEATURE: mermaid docs
Mermaid provides the missing link between text-based wikis and images. You can easily insert simple flow diagrams without having to manage source images, and as they're embedded in the markdown itself they are version controlled.

Try it here: https://mermaid.live/
-->

https://daringfireball.net/projects/markdown/syntax
